package accessingdatamysql;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity // This tells Hibernate to make a table out of this class
public class Evenement {
	  @Id
	  @GeneratedValue(strategy=GenerationType.AUTO)
	  private Integer id;
	  
	  
	  public String name;
	  
	  
	  public Evenement() {
		super();
	}
	  
	 @OneToMany(mappedBy="evenement")
	 private List<User> participants = new ArrayList<>();
	 
	 public List<User> addParticipant(User user) {
		 this.participants.add(user);
		return participants;
	 }
	
	public Evenement(String nom) {
		super();
		this.name = nom;
	}
	


	public String getName() {
		return this.name;
	}
	 
	public Integer getId() {
		return this.id;
	}
	
	public void setNom(String nom) {
		this.name = nom;
	}
}
