package accessingdatamysql;

import org.springframework.data.repository.CrudRepository;

import accessingdatamysql.Evenement;

public interface EvenementRepository extends CrudRepository<Evenement, Integer> {

}
