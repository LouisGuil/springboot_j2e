# Projet Spring

## Installation 

- Dans un premier temps installer java :

sudo apt install openjdk8-jdk

- Ensuite il faut installer la base de donnée : mysql

sudo apt install mysql-server

- Création de la base de donnée

sudo mysql --password
mysql> create database participant; -- Création de la base de donnée

mysql> create user 'springuser'@'%' identified by formation20192020; -- Création de l'utilisateur

mysql> grant all on participant.* to 'springuser'@'%'; -- Donne tous les droits à l'utilisateur.

## Lancement de l'application avec un IDE

Vous pouvez l'ancer l'application avec NetBeans ou Eclipse.

## Utilisation

Rendez-vous à l'adresse localhost:8080, sur cette page vous pouvez ajouter un évenement (à faire en premier sinon on ne peut pas ajouter d'utilisateur pour celui ci). Vous pouvez ajouter un utilisateur, afficher tous les utilisateurs.

J'ai eu des problèmes pour la suppression d'un utilisateur, en effet celui ci est relié à un evènement. Il faut supprimer l'evenement avant de supprimer l'utilisateur.